﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using DynamicModel.Core.Web.UI.Models;
using System.Globalization;

namespace DynamicModel.Core.Web.UI.Controllers
{
    public class AccountController : Controller
    {
        private readonly IStringLocalizer<AccountController> localizer;
        public AccountController(IStringLocalizer<AccountController> localizer)
        {
            this.localizer = localizer;
        }

        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Register(AccountRegister model)
        {
            if (ModelState.IsValid)
            {
                //Register account through API and return token
                return RedirectToAction("Login");
            }
            else
            {
                model.Password = null;
                model.PasswordConfirm = null;
                return View(model);
            }
        }

        public IActionResult Login()
        {
            return View();
        }
    }
}