﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DynamicModel.Core.Web.UI.Models;
using Microsoft.AspNetCore.Mvc;

namespace DynamicModel.Core.Web.UI.Controllers
{
    public class HealthCheckController : Controller
    {
        public IActionResult Status()
        {
            //Sample model
            List<HealthCheck> models = new List<HealthCheck>()
            {
                new HealthCheck(){Name="Database connectivity check", Healthy=true},
                new HealthCheck(){Name="Redis connectivity check", Healthy=false}
            };

            return View(models);
        }
    }
}