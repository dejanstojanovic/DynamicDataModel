﻿using DynamicModel.Core.Web.UI.Validation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DynamicModel.Core.Web.UI.Models
{
    public class AccountRegister : GoogleReCaptchaModelBase
    {
        [Required]
        public String Username { get; set; }

        [Required]
        public String Password { get; set; }

        [Required]
        public String PasswordConfirm { get; set; }
    }
}
