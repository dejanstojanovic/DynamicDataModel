﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DynamicModel.Core.Web.UI.Models
{
    public class HealthCheck
    {
        public String Name { get; set; }
        public Boolean Healthy { get; set; }
    }
}
