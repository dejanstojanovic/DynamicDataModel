﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper.Configuration;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.HealthChecks;
using Microsoft.Extensions.Logging;

namespace DynamicModel.Core.Web.Api.Controllers
{
    [ApiVersion("1.0")]
    [Produces("application/json")]
    [Route("api/v{version:apiVersion}/HealthChecks")]
    public class HealthChecksController : Controller
    {
        #region Fields
        protected readonly IConfiguration configuration;
        protected readonly ILogger logger;
        protected readonly IMemoryCache memoryCache;
        protected readonly IDistributedCache distributedCache;
        protected readonly UserManager<IdentityUser> userManager;
        private readonly IHealthCheckService healthCheckService;
        #endregion



        public HealthChecksController(
            UserManager<IdentityUser> userManager,
            IConfiguration configuration,
            IMemoryCache memoryCache,
            IDistributedCache distributedCache,
            ILogger<DataTypesController> logger,
            IHealthCheckService healthCheckService)
        {
            this.healthCheckService = healthCheckService;
            this.userManager = userManager;
            this.configuration = configuration;
            this.memoryCache = memoryCache;
            this.logger = logger;
            this.distributedCache = distributedCache;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            CompositeHealthCheckResult healthCheckResult = await this.healthCheckService.CheckHealthAsync();

            var response = healthCheckResult.Results.Select(r => new { Name = r.Key, Healthy = r.Value.CheckStatus == CheckStatus.Healthy });
            if(healthCheckResult.CheckStatus != CheckStatus.Healthy)
            {
                return new JsonResult(response) { StatusCode = StatusCodes.Status500InternalServerError };
            }
            return new JsonResult(response){ StatusCode= StatusCodes.Status200OK};


        }

    }
}