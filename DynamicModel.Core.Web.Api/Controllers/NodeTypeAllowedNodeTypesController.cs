﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DynamicModel.Core.Data.Entities.Graph;
using DynamicModel.Core.Data.Repositories.UnitsOfWork;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace DynamicModel.Core.Web.Api.Controllers
{
    [ApiVersion("1.0")]
    [ApiVersion("0.9", Deprecated = true)]
    [Produces("application/json")]
    [Route("api/NodeTypeAllowedNodeTypes")]
    [Authorize]
    public class NodeTypeAllowedNodeTypesController : BaseController<NodeTypeAllowedNodeType>
    {
        public NodeTypeAllowedNodeTypesController(
            UserManager<IdentityUser> userManager,
            IUnitOfWork unitOfWork, 
            IConfiguration configuration, 
            IMemoryCache memoryCache, 
            ILogger<DataTypesController> logger) : base(userManager,unitOfWork, configuration, memoryCache, logger)
        {
        }

        [HttpGet]
        public IEnumerable<NodeTypeAllowedNodeType> Get()
        {
            using (this.UnitOfWork)
            {
                return this.UnitOfWork.NodeTypeAllowedNodeTypes.GetPaged(0, int.MaxValue).ToList();
            }
        }

        [HttpGet("{id}")]
        public NodeTypeAllowedNodeType Get(Guid id)
        {
            using (this.UnitOfWork)
            {
                return this.UnitOfWork.NodeTypeAllowedNodeTypes.GetById(id);
            }
        }

        [HttpDelete("{id}")]
        public void Delete([FromBody]Guid id)
        {
            using (this.UnitOfWork)
            {
                this.UnitOfWork.NodeTypeAllowedNodeTypes.Remove(id);
                this.UnitOfWork.Save();
            }
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody]NodeTypeAllowedNodeType entity)
        {
            using (this.UnitOfWork)
            {
                entity.Id = base.GetIdentity(entity);
                this.UnitOfWork.NodeTypeAllowedNodeTypes.Insert(entity);
                await this.UnitOfWork.SaveAsync();
                return CreatedAtAction("Get", new { id = entity.Id });
            }
        }
    }
}