﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DynamicModel.Core.Data.Repositories.UnitsOfWork;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using DynamicModel.Core.Web.Api.Models;
using AutoMapper;
using Swashbuckle.AspNetCore.SwaggerGen;
using Microsoft.Extensions.Caching.Distributed;

namespace DynamicModel.Core.Web.Api.Controllers
{
    [ApiVersion("1.0")]
    [ApiVersion("0.9", Deprecated = true)]
    [Produces("application/json")]
    [Route("api/v{version:apiVersion}/PropertyTypes")]
    [Authorize]
    [SwaggerResponse(StatusCodes.Status500InternalServerError, typeof(ErrorModel))]
    [SwaggerResponse(StatusCodes.Status400BadRequest, typeof(ErrorModel))]
    public class PropertyTypesController : BaseController<PropertyType>
    {
        public PropertyTypesController(
            UserManager<IdentityUser> userManager,
            IUnitOfWork unitOfWork,
            IMapper mapper,
            IConfiguration configuration,
            IMemoryCache memoryCache,
            IDistributedCache distributedCache,
            ILogger<PropertyTypesController> logger) : base(userManager, unitOfWork, mapper, configuration, memoryCache,distributedCache, logger)
        {
        }

        [HttpGet]
        public IEnumerable<PropertyType> Get()
        {
            IEnumerable<Data.Entities.Graph.PropertyType> entities;
            using (this.UnitOfWork)
            {
                entities = this.UnitOfWork.PropertyTypes.GetPaged(0, int.MaxValue).ToList();
            }
            return this.mapper.Map<IEnumerable<PropertyType>>(entities);
        }

        [HttpGet("{id}")]
        public async Task<PropertyType> Get(Guid id)
        {
            Data.Entities.Graph.PropertyType entity;
            using (this.UnitOfWork)
            {
                entity = await this.UnitOfWork.PropertyTypes.GetByIdAsync(id);
            }
            return this.mapper.Map<PropertyType>(entity);

        }

        [HttpDelete("{id}")]
        public async Task Delete([FromBody]Guid id)
        {
            using (this.UnitOfWork)
            {
                this.UnitOfWork.NodeTypes.Remove(id);
               await this.UnitOfWork.SaveAsync();
            }
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody]PropertyType model)
        {
            using (this.UnitOfWork)
            {
                model.Id = base.GetIdentity(model);
                var entity = this.mapper.Map<Data.Entities.Graph.PropertyType>(model);
                await this.UnitOfWork.PropertyTypes.InsertAsync(entity);
                await this.UnitOfWork.SaveAsync();                
                return CreatedAtAction("Get", new { id = model.Id });
            }
        }

        [HttpPut]
        public async Task Put([FromBody]PropertyType model)
        {
            using (this.UnitOfWork)
            {

                var entity = this.mapper.Map<Data.Entities.Graph.PropertyType>(model);
                this.UnitOfWork.Update(entity);
                await this.UnitOfWork.SaveAsync();
            }
        }


        [Route("{propertyTypeId}/Properties")]
        [HttpGet]
        public IEnumerable<Property> PropertiesGet(Guid propertyTypeId)
        {
            IEnumerable<Data.Entities.Graph.Property> entities;
            using (this.UnitOfWork)
            {
                entities= this.UnitOfWork.Properties.Find(p => p.PropertyTypeId == propertyTypeId).ToList();
            }
            return this.mapper.Map<IEnumerable<Property>>(entities);
        }

        [Route("{propertyTypeId}/DataType")]
        [HttpGet]
        public async Task<DataType> DataTypeGet(Guid propertyTypeId)
        {
            Data.Entities.Graph.PropertyType entity;
            using (this.UnitOfWork)
            {
                entity = await this.UnitOfWork.PropertyTypes.GetByIdAsync(propertyTypeId);
            }
            return this.mapper.Map<DataType>(entity.DataType);
        }
    }
}