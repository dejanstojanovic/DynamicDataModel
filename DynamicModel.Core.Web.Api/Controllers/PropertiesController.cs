﻿using AutoMapper;
using DynamicModel.Core.Data.Repositories.UnitsOfWork;
using DynamicModel.Core.Web.Api.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DynamicModel.Core.Web.Api.Controllers
{
    [ApiVersion("1.0")]
    [ApiVersion("0.9", Deprecated = true)]
    [Produces("application/json")]
    [Route("api/v{version:apiVersion}/Properies")]
    [Authorize]
    [SwaggerResponse(StatusCodes.Status500InternalServerError, typeof(ErrorModel))]
    [SwaggerResponse(StatusCodes.Status400BadRequest, typeof(ErrorModel))]
    public class PropertiesController : BaseController<Property>
    {
        protected PropertiesController(
            UserManager<IdentityUser> userManager, 
            IUnitOfWork unitOfWork, 
            IMapper mapper, 
            IConfiguration configuration, 
            IMemoryCache memoryCache,
            IDistributedCache distributedCache,
            ILogger<PropertiesController> logger) 
            : base(userManager, unitOfWork, mapper, configuration, memoryCache,distributedCache, logger)
        {
        }


    }
}
