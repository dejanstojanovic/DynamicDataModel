﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DynamicModel.Core.Data.Repositories.UnitsOfWork;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using AutoMapper;
using DynamicModel.Core.Web.Api.Models;
using Swashbuckle.AspNetCore.SwaggerGen;
using Microsoft.Extensions.Caching.Distributed;

namespace DynamicModel.Core.Web.Api.Controllers
{
    [ApiVersion("1.0")]
    [ApiVersion("0.9", Deprecated = true)]
    [Produces("application/json")]
    [Route("api/v{version:apiVersion}/NodeTypes")]
    [Authorize]
    [SwaggerResponse(StatusCodes.Status500InternalServerError, typeof(ErrorModel))]
    [SwaggerResponse(StatusCodes.Status400BadRequest, typeof(ErrorModel))]
    public class NodeTypesController : BaseController<NodeType>
    {
        public NodeTypesController(
            UserManager<IdentityUser> userManager,
            IUnitOfWork unitOfWork,
            IMapper mapper,
            IConfiguration configuration, 
            IMemoryCache memoryCache,
            IDistributedCache distributedCache,
            ILogger<NodeTypesController> logger) : base(userManager,unitOfWork,mapper, configuration, memoryCache,distributedCache, logger)
        {
        }


        [HttpGet]
        public IEnumerable<NodeType> Get()
        {
            IEnumerable<Data.Entities.Graph.NodeType> entities;
            using (this.UnitOfWork)
            {
                entities = this.UnitOfWork.NodeTypes.GetPaged(0, int.MaxValue).ToList();
            }
            return this.mapper.Map<IEnumerable<NodeType>>(entities);
        }

        [HttpGet("{id}")]
        public async Task<NodeType> Get(Guid id)
        {
            Data.Entities.Graph.NodeType entity;
            using (this.UnitOfWork)
            {
                entity = await this.UnitOfWork.NodeTypes.GetByIdAsync(id);
            }
            return this.mapper.Map<NodeType>(entity);
        }

        [HttpDelete("{id}")]
        public async Task Delete([FromBody]Guid id)
        {
            using (this.UnitOfWork)
            {
                this.UnitOfWork.NodeTypes.Remove(id);
                await this.UnitOfWork.SaveAsync();
            }
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody]NodeType model)
        {
            using (this.UnitOfWork)
            {
                model.Id = base.GetIdentity(model);
                var entity = this.mapper.Map<Data.Entities.Graph.NodeType>(model);
                await this.UnitOfWork.NodeTypes.InsertAsync(entity);
                await this.UnitOfWork.SaveAsync();

                return CreatedAtAction("Get", new { id = model.Id });
            }
        }

        [HttpPut]
        public async Task Put([FromBody]NodeType model)
        {
            using (this.UnitOfWork)
            {
                var entity = this.mapper.Map<Data.Entities.Graph.NodeType>(model);
                this.UnitOfWork.Update(entity);
                await this.UnitOfWork.SaveAsync();
            }
        }

        [Route("{nodeTypeId}/PropertyTypes")]
        [HttpGet]
        public IEnumerable<PropertyType> PropertyTypesGet(Guid nodeTypeId)
        {
            IEnumerable<Data.Entities.Graph.PropertyType> entities;
            using (this.UnitOfWork)
            {
                entities = this.UnitOfWork.PropertyTypes.Find(p => p.NodeTypeId == nodeTypeId).ToList();
            }
            return this.mapper.Map<IEnumerable<PropertyType>>(entities);
        }

        [Route("{nodeTypeId}/Nodes")]
        [HttpGet]
        public IEnumerable<Node> NodesGet(Guid nodeTypeId)
        {
            IEnumerable<Data.Entities.Graph.Node> entities;
            using (this.UnitOfWork)
            {
                entities = this.UnitOfWork.Nodes.Find(p => p.NodeTypeId == nodeTypeId).ToList();
            }
            return this.mapper.Map<IEnumerable<Node>>(entities);
        }

        [Route("{nodeTypeId}/AllowedNodeTypes")]
        [HttpGet]
        public IEnumerable<NodeType> AllowedNodeTypesGet(Guid nodeTypeId)
        {
            IEnumerable<Data.Entities.Graph.NodeType> entities;
            using (this.UnitOfWork)
            {
                entities = this.UnitOfWork.NodeTypeAllowedNodeTypes.Find(t => t.NodeTypeId.Equals(nodeTypeId)).Select(t => t.AllowedNodeType);
            }
            return this.mapper.Map<IEnumerable<NodeType>>(entities);
        }
    }
}