﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DynamicModel.Core.Data.Exceptions;
using DynamicModel.Core.Data.Repositories.UnitsOfWork;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using DynamicModel.Core.Web.Api.Models;
using AutoMapper;
using Swashbuckle.AspNetCore.SwaggerGen;
using Microsoft.Extensions.Caching.Distributed;
using DynamicModel.Core.Common.Extensions;

namespace DynamicModel.Core.Web.Api.Controllers
{
    [ApiVersion("1.0")]
    [ApiVersion("0.9", Deprecated = true)]
    [Produces("application/json")]
    [Route("api/v{version:apiVersion}/DataTypes")]
    //[Authorize]
    [SwaggerResponse(StatusCodes.Status500InternalServerError, typeof(ErrorModel))]
    [SwaggerResponse(StatusCodes.Status400BadRequest, typeof(ErrorModel))]
    public class DataTypesController : BaseController<DataType>
    {
        public DataTypesController(
            UserManager<IdentityUser> userManager,
            IUnitOfWork unitOfWork,
            IMapper mapper,
            IConfiguration configuration,
            IMemoryCache memoryCache,
            IDistributedCache distributedCache,
            ILogger<DataTypesController> logger) : base(userManager,unitOfWork,mapper, configuration, memoryCache, distributedCache , logger)
        {
        }

        [HttpGet]
        public async Task<IEnumerable<DataType>> Get()
        {
            IEnumerable<Data.Entities.Graph.DataType> entities;
            using (this.UnitOfWork)
            {
                entities= this.UnitOfWork.DataTypes.GetPaged(0, int.MaxValue).ToList();
            }

            var result = this.mapper.Map<IEnumerable<Data.Entities.Graph.DataType>, IEnumerable<DataType>>(entities);         
            return await Task<IEnumerable<DataType>>.FromResult(result);
        }

        [HttpGet("{id}")]
        public async Task<DataType> Get(Guid id)
        {
            Data.Entities.Graph.DataType entity;
            using (this.UnitOfWork)
            {
                entity = await this.UnitOfWork.DataTypes.GetByIdAsync(id);
            }
           return this.mapper.Map<DataType>(entity);
        }

        [HttpDelete("{id}")]
        public void Delete([FromBody]Guid id)
        {
            using (this.UnitOfWork)
            {
                this.UnitOfWork.DataTypes.Remove(id);
                this.UnitOfWork.Save();
            }
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody]DataType model)
        {
            using (this.UnitOfWork)
            {

                if (ModelState.ErrorCount == 0)
                {

                    var entity = this.mapper.Map<DataType, Data.Entities.Graph.DataType>(model);
                    entity.Id = base.GetIdentity(model);
                    await this.UnitOfWork.DataTypes.InsertAsync(entity);
                    await this.UnitOfWork.SaveAsync();

                    return CreatedAtAction("Get", new { id = entity.Id });
                }
                else
                {
                    Response.StatusCode = StatusCodes.Status400BadRequest;
                    return new JsonResult(new ErrorModel(new System.ComponentModel.DataAnnotations.ValidationException(String.Join(Environment.NewLine, ModelState.Select(m => m.Value).ToList()))));
                }
            }
        }

        [HttpPut]
        public async Task<IActionResult> Put([FromBody]DataType model)
        {
            using (this.UnitOfWork)
            {
                if (ModelState.ErrorCount == 0)
                {
                    Data.Entities.Graph.DataType entity = this.mapper.Map<Data.Entities.Graph.DataType>(model);
                    this.UnitOfWork.Update(entity);
                    await this.UnitOfWork.SaveAsync();
                    Response.StatusCode = StatusCodes.Status204NoContent;
                    return null;
                }
                else
                {
                    Response.StatusCode = StatusCodes.Status400BadRequest;
                    return new JsonResult(new ErrorModel(new System.ComponentModel.DataAnnotations.ValidationException(String.Join(Environment.NewLine, ModelState.Select(m => m.Value).ToList()))));
                }
            }
        }

        [Route("{dataTypeId}/PropertyTypes")]
        [HttpGet]
        public IEnumerable<PropertyType> PropertyTypesGet(Guid dataTypeId)
        {
            IEnumerable<Data.Entities.Graph.PropertyType> entities;
            using (this.UnitOfWork)
            {
                entities = this.UnitOfWork.PropertyTypes.Find(p => p.DataTypeId == dataTypeId).ToList();
            }
            return this.mapper.Map<IEnumerable<PropertyType>>(entities);
        }


    }
}