﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DynamicModel.Core.Data.Migrations;
using DynamicModel.Data.Repositories.Defintions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace DynamicModel.Core.Web.Api.Controllers
{
    [Route("api/[controller]")]
    public class ValuesController : Controller
    {
        IConfiguration configuration;
        ILogger logger;

        public ValuesController(IConfiguration configuration,ILogger<ValuesController> logger)
        {
            this.configuration = configuration;
            this.logger = logger;
            logger.LogInformation("Constructor log");
        }


        // GET api/values
        [HttpGet]
        public IEnumerable<string> Get()
        {

            return new String[] { "One", "Two" };

            //using(var dataConfigRepo = new ObjectPropertyDataDefinitionsRepository(this.configuration))
            //{
            //    return dataConfigRepo.GetPaged(0, 10).Select(d => d.Name).ToArray();
            //}
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
