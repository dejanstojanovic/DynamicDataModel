﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DynamicModel.Core.Data.Entities;
using DynamicModel.Core.Data.Repositories.UnitsOfWork;
using DynamicModel.Core.Web.Api.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace DynamicModel.Core.Web.Api.Controllers
{
   public abstract class BaseController<T> : Controller where T : BaseModel
    {
        #region Fields
        protected readonly IConfiguration configuration;
        protected readonly ILogger logger;
        protected readonly IUnitOfWork untiOfWork;
        protected readonly IMemoryCache memoryCache;
        protected readonly IDistributedCache distributedCache;
        protected readonly IMapper mapper;
        protected readonly UserManager<IdentityUser> userManager;
        #endregion

        #region Constructors

        protected BaseController(
            UserManager<IdentityUser> userManager,
            IUnitOfWork unitOfWork,
            IMapper mapper,
            IConfiguration configuration, 
            IMemoryCache memoryCache,
            IDistributedCache distributedCache,
            ILogger<BaseController<T>> logger)
        {
            this.userManager = userManager;
            this.untiOfWork = unitOfWork;
            this.configuration = configuration;
            this.memoryCache = memoryCache;
            this.logger = logger;
            this.mapper = mapper;
            this.distributedCache = distributedCache;
        }
        #endregion


        #region Properties
        public IConfiguration Configuration => this.configuration;
        public ILogger Logger => this.logger;
        public IUnitOfWork UnitOfWork => this.untiOfWork;
        #endregion


        #region Abstract methods

        protected Guid GetIdentity(T model)
        {
            return model.Id == null || model.Id == Guid.Empty ? Guid.NewGuid() : model.Id;
        }

        #endregion

    }
}