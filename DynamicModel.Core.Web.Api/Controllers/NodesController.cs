﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DynamicModel.Core.Data.Repositories.UnitsOfWork;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using AutoMapper;
using DynamicModel.Core.Web.Api.Models;
using Swashbuckle.AspNetCore.SwaggerGen;
using Microsoft.Extensions.Caching.Distributed;

namespace DynamicModel.Core.Web.Api.Controllers
{
    [ApiVersion("1.0")]
    [ApiVersion("0.9", Deprecated = true)]
    [Produces("application/json")]
    [Route("api/v{version:apiVersion}/Nodes")]
    [Authorize]
    [SwaggerResponse(StatusCodes.Status500InternalServerError, typeof(ErrorModel))]
    [SwaggerResponse(StatusCodes.Status400BadRequest, typeof(ErrorModel))]
    public class NodesController : BaseController<Node>
    {
        public NodesController(
            UserManager<IdentityUser> userManager,
            IUnitOfWork unitOfWork,
            IMapper mapper,
            IConfiguration configuration, 
            IMemoryCache memoryCache,
            IDistributedCache distributedCache,
            ILogger<NodesController> logger) : base(userManager,unitOfWork,mapper, configuration, memoryCache, distributedCache, logger)
        {
        }

        [HttpGet]
        public IEnumerable<Node> Get()
        {
            IEnumerable<Data.Entities.Graph.Node> entities;
            using (this.UnitOfWork)
            {
                entities = this.UnitOfWork.Nodes.GetPaged(0, int.MaxValue).ToList();
            }

            return this.mapper.Map<IEnumerable<Node>>(entities);
        }

        [HttpGet("{id}")]
        public async Task<Node> Get(Guid id)
        {
            Data.Entities.Graph.Node entity;
            using (this.UnitOfWork)
            {
                entity = await this.UnitOfWork.Nodes.GetByIdAsync(id);
            }
            return this.mapper.Map<Node>(entity);
        }

        [HttpDelete("{id}")]
        public async Task Delete([FromBody]Guid id)
        {
            using (this.UnitOfWork)
            {
                this.UnitOfWork.Nodes.Remove(id);
               await this.UnitOfWork.SaveAsync();
            }
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody]Node model)
        {
            using (this.UnitOfWork)
            {
                model.Id = base.GetIdentity(model);
                var entity = this.mapper.Map<Data.Entities.Graph.Node>(model);

                await this.UnitOfWork.Nodes.InsertAsync(entity);
                await this.UnitOfWork.SaveAsync();
                return CreatedAtAction("Get", new { id = model.Id });
            }
        }

        [HttpPut]
        public async Task Put([FromBody]Node model)
        {
            using (this.UnitOfWork)
            {
                var entity = this.mapper.Map<Data.Entities.Graph.Node>(model);
                this.UnitOfWork.Update(entity);
                await this.UnitOfWork.SaveAsync();
            }
        }


        [Route("{nodeId}/Properties")]
        [HttpGet]
        public IEnumerable<Property> PropertiesGet(Guid nodeId)
        {
            IEnumerable<Data.Entities.Graph.Property> entities;
            using (this.UnitOfWork)
            {
                entities= this.UnitOfWork.Properties.Find(p => p.NodeId == nodeId).ToList();
            }
            return this.mapper.Map<IEnumerable<Property>>(entities);
        }

        [Route("{nodeId}/Children")]
        [HttpGet]
        public async Task<IEnumerable<Node>> Children(Guid nodeId)
        {
            Data.Entities.Graph.Node entity;
            using (this.UnitOfWork)
            {
                entity = await this.UnitOfWork.Nodes.GetByIdAsync(nodeId);
            }
            return this.mapper.Map<IEnumerable<Node>>(entity.Children.ToList());
        }

        [Route("{nodeId}/Descendants")]
        [HttpGet]
        public async Task<IEnumerable<Node>> Descendants(Guid nodeId)
        {
            Data.Entities.Graph.Node entity;
            using (this.UnitOfWork)
            {
                entity = await this.UnitOfWork.Nodes.GetByIdAsync(nodeId);
            }
            return this.mapper.Map<IEnumerable<Node>>(entity.Children.SelectMany(c => c.Children).ToList());
        }

        [Route("{nodeId}/Ancestors")]
        [HttpGet]
        public IEnumerable<Node> Ancestors(Guid nodeId)
        {
            IList<Data.Entities.Graph.Node> ancestors = new List<Data.Entities.Graph.Node>();
            using (this.UnitOfWork)
            {
               void CalculateDescendants(Data.Entities.Graph.Node node)
                {
                    var parent = node.Parent;
                    ancestors.Add(parent);
                    if (parent != null)
                    {
                        CalculateDescendants(parent);
                    }
                }
            }
            return this.mapper.Map<IEnumerable<Node>>(ancestors);
        }

        [Route("{nodeId}/NodeType")]
        [HttpGet]
        public async Task<NodeType> NodeTypeGet(Guid nodeId)
        {
            Data.Entities.Graph.Node entity;
            using (this.UnitOfWork)
            {
                entity = await this.UnitOfWork.Nodes.GetByIdAsync(nodeId);
            }
            return this.mapper.Map<NodeType>(entity.NodeType);
        }


        [Route("{nodeId}/AllowedNodeTypes")]
        [HttpGet]
        public async Task<IEnumerable<NodeType>> AllowedNodeTypesGet(Guid nodeId)
        {
            IEnumerable<Data.Entities.Graph.NodeType> entities;
            using (this.UnitOfWork)
            {
                var node = await this.untiOfWork.Nodes.GetByIdAsync(nodeId);
                entities = node.NodeType
                    .NodeTypeAllowedNodeTypes.Where(t=>!t.MultipleOccurences && node.Children.Any(c=>c.NodeType.Id.Equals(t.Id))) //Exclude types which are alredy added and can be added only once
                    .Select(n => n.AllowedNodeType);
            }
            return this.mapper.Map<IEnumerable<NodeType>>(entities);
        }
    }
}