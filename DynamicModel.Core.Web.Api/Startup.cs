﻿using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using DynamicModel.Core.Data.Entities;
using DynamicModel.Core.Data.Repositories.UnitsOfWork;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Serilog;
using Swashbuckle.AspNetCore.Swagger;
using AutoMapper;
using DynamicModel.Core.Web.Api.Models;
using Microsoft.AspNetCore.Mvc.Versioning;
using Microsoft.Extensions.HealthChecks;
using DynamicModel.Core.Common.Extensions;
using System;

namespace DynamicModel.Core.Web.Api
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        public IHostingEnvironment HostingEnvironment { get; }

        public Startup(IConfiguration configuration, IHostingEnvironment env)
        {
            Configuration = configuration;
            HostingEnvironment = env;
            Log.Logger = new LoggerConfiguration()
                                .ReadFrom.Configuration(configuration)
                                .CreateLogger();
        }



        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            #region Add CORS
            services.AddCors(options => options.AddPolicy("Cors", builder =>
            {
                builder
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader();
            }));
            #endregion

            #region Add Entity Framework and Identity Framework

            services.AddDbContext<DynamicDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DynamicModelConnection")));

            services.AddDbContext<UserDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DynamicModelConnection")));

            services.AddIdentity<IdentityUser, IdentityRole>()
                .AddEntityFrameworkStores<UserDbContext>();

            services.AddScoped<IUnitOfWork, DynamicDataUnitOfWork>();
            #endregion

            #region Add caching

            services.AddMemoryCache();

            #endregion

            #region Add Authentication
            var signingKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Tokens:Key"]));
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(config =>
            {
                config.RequireHttpsMetadata = false;
                config.SaveToken = true;
                config.TokenValidationParameters = new TokenValidationParameters()
                {
                    IssuerSigningKey = signingKey,
                    ValidateAudience = true,
                    ValidAudience = this.Configuration["Tokens:Audience"],
                    ValidateIssuer = true,
                    ValidIssuer = this.Configuration["Tokens:Issuer"],
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true
                };
            });
            #endregion

            #region Add MVC
            services.AddMvc(options => { }).AddJsonOptions(options =>
            {
                options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            }); ;

            #endregion

            #region Add Swagger

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = this.GetType().Namespace, Version = "v1" });

                #region Authorization
                var security = new Dictionary<string, IEnumerable<string>>
                    {
                        {"Bearer", new string[] { }},
                    };

                c.AddSecurityDefinition("Bearer", new ApiKeyScheme
                {
                    Description = "JWT Authorization header using the Bearer scheme. Example: \"Authorization: Bearer {token}\"",
                    Name = "Authorization",
                    In = "header",
                    Type = "apiKey"
                });
                c.AddSecurityRequirement(security);
                #endregion
            });

            #endregion

            #region Add Logging
            services.AddLogging();
            #endregion

            #region Add Configuration
            services.AddSingleton<IConfiguration>(new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile($"appsettings.{this.HostingEnvironment.EnvironmentName.ToLower()}.json")
                .Build());
            #endregion

            #region Add versioning
            services.AddApiVersioning(o => o.ReportApiVersions = true);
            //services.AddApiVersioning(o => o.ApiVersionReader = new HeaderApiVersionReader("api-version"));
            #endregion

            #region Add AutoMapper
            services.AddSingleton(provider => new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new MappingProfile());
            }).CreateMapper());
            #endregion

            #region Add Redis distributed cache
            services.AddDistributedRedisCache(option =>
            {
                option.Configuration = $"{this.Configuration.Get<string>("Redis:Host")}:{this.Configuration.Get<string>("Redis:Port")}";
                option.InstanceName = this.Configuration.Get<string>("Redis:Instance");
            });
            #endregion

            #region Add healthchecks
            services.AddHealthChecks(checks =>
            {
                checks.AddSqlCheck("Database Connectivity Check", Configuration.GetConnectionString("DynamicModelConnection"),TimeSpan.FromSeconds(10));
                checks.AddRedisCheck("Redis Connectivity Check", TimeSpan.FromSeconds(10), this.Configuration.Get<string>("Redis:Host"), this.Configuration.Get<int>("Redis:Port"));
            });
            #endregion
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            app.UseCors("Cors");

            app.UseAuthentication();

            loggerFactory.AddSerilog();

            if (env.IsDevelopment())
            {
                //app.UseDeveloperExceptionPage();

                #region Error handling
                app.UseExceptionHandler(x =>
                {
                    x.Run(async context =>
                    {
                        context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                        context.Response.ContentType = "application/json";
                        var ex = context.Features.Get<IExceptionHandlerFeature>();
                        if (ex != null)
                        {
                            await context.Response.WriteAsync(
                                new ErrorModel(ex.Error).ToJson()
                                ).ConfigureAwait(false);
                        }
                    });
                });

                #endregion

                loggerFactory.AddDebug(LogLevel.Information);
            }
            else
            {
                #region Error handling
                app.UseExceptionHandler(x =>
                {
                    x.Run(async context =>
                    {
                        context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                        context.Response.ContentType = "application/json";
                        await context.Response.WriteAsync(string.Empty).ConfigureAwait(false);
                    });
                });
                #endregion

                loggerFactory.AddDebug(LogLevel.Warning);
            }

            #region Configure Swagger

            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", this.GetType().Namespace);
            });

            #endregion

            app.UseMvc();

        }
    }
}
