﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DynamicModel.Core.Web.Api.Models
{
    internal class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Data.Entities.Graph.DataType, DataType>();
            CreateMap<DataType, Data.Entities.Graph.DataType>();

            CreateMap<Data.Entities.Graph.PropertyType, PropertyType>();
            CreateMap<PropertyType, Data.Entities.Graph.PropertyType>();

            CreateMap<Data.Entities.Graph.NodeType, NodeType>();
            CreateMap<NodeType, Data.Entities.Graph.NodeType>();

            CreateMap<Data.Entities.Graph.Node, Node>();
            CreateMap<Node, Data.Entities.Graph.Node>();

            CreateMap<Data.Entities.Graph.Property, Property>();
            CreateMap<Property, Data.Entities.Graph.Property>();
        }
    }
}
