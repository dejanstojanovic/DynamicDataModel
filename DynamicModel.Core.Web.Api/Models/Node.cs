﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DynamicModel.Core.Web.Api.Models
{
    [Serializable]
    public class Node: BaseModel
    {
        public Guid? ParentId { get; set; }
    }
}
