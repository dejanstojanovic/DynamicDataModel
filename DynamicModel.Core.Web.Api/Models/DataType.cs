﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DynamicModel.Core.Web.Api.Models
{
    [Serializable]
    public class DataType: BaseModel
    {
        [Required]
        [MaxLength(250)]
        public String SystemType { get; set; }

        [MaxLength(250)]
        public String Description { get; set; }
    }
}
