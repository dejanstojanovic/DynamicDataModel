﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DynamicModel.Core.Web.Api.Models
{
    [Serializable]
    public class Property : BaseModel
    {
        public String NodeId { get; set; }
        public String Value { get; set; }

    }
}
