﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DynamicModel.Core.Web.Api.Models
{
    [Serializable]
    public abstract class BaseModel
    {
        [Required]
        public virtual Guid Id { get; set; }

        [Required]
        [MaxLength(100)]
        [RegularExpression(@"[^\w\s]")]
        public String Name { get; set; }
    }
}
