﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DynamicModel.Core.Web.Api.Models
{
    [Serializable]
    public class HealthCheck
    {
        public String Name { get; set; }
        public Boolean Healthy { get; set; }
    }
}
