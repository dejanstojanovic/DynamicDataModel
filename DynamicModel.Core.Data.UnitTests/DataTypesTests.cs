using DynamicModel.Core.Data.Entities;
using DynamicModel.Core.Data.Repositories.UnitsOfWork;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace DynamicModel.Core.Data.UnitTests
{
    [TestClass]
    public class DataTypesTests
    {
        private const String inMemoryDbName = "TestDatabase";

        private DynamicDbContext GetDbContext()
        {
            return new DynamicDbContext(new DbContextOptionsBuilder<DynamicDbContext>()
                .UseInMemoryDatabase(databaseName: inMemoryDbName)
                .Options);
        }

        [TestMethod]
        public async Task AddDataType()
        {
            using (var dbContext = GetDbContext())
            {
                using(var unitOfWork = new DynamicDataUnitOfWork(dbContext))
                {
                    await unitOfWork.DataTypes.InsertAsync(new Entities.Graph.DataType()
                    {
                       Name= "String",
                       SystemType ="System.String",

                    });

                    await unitOfWork.SaveAsync();

                    Assert.IsTrue(unitOfWork.DataTypes.GetPaged(0, int.MaxValue).Count() > 0);
                }
            }
        }

        [TestMethod]
        public async Task GetDataType()
        {
            using (var dbContext = GetDbContext())
            {
                using (var unitOfWork = new DynamicDataUnitOfWork(dbContext))
                {
                    var newDataType = new Entities.Graph.DataType()
                    {
                        Id = Guid.NewGuid(),
                        Name = "String",
                        SystemType = "System.String",

                    };

                    await unitOfWork.DataTypes.InsertAsync(newDataType);

                    await unitOfWork.SaveAsync();

                    Assert.IsTrue(unitOfWork.DataTypes.GetPaged(0, int.MaxValue).Count() > 0);
                    Assert.IsTrue(await unitOfWork.DataTypes.GetByIdAsync(newDataType.Id)!=null);

                }
            }
        }

    }
}
