﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DynamicModel.Core.Web.IdentityServer.Models
{
    public class Credentials
    {
        public String Username { get; set; }
        public String Password { get; set; }
    }
}
