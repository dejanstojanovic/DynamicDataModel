﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DynamicModel.Core.Web.Api.Graphql.Schema
{
    public interface IService<T>
    {
        Task<T> GetByIdAsync(Guid Id);

        Task<IEnumerable<T>> GetAsync();
    }
}
