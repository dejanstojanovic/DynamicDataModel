﻿using DynamicModel.Core.Data.Entities.Graph;
using DynamicModel.Core.Data.Repositories.UnitsOfWork;
using GraphQL.Types;


namespace DynamicModel.Core.Web.Api.Graphql.Schema
{
    public class DataTypeType : ObjectGraphType<DataType>
    {
        public DataTypeType(IUnitOfWork unitOfWork)
        {
            Field(d => d.Id);
            Field(d => d.Name);
            Field(d => d.SystemType);
            Field(d => d.Description);
        }
    }
}
