﻿using DynamicModel.Core.Data.Entities.Graph;
using DynamicModel.Core.Data.Repositories.UnitsOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DynamicModel.Core.Web.Api.Graphql.Schema
{
    public class DataTypeService : IService<DataType>
    {
        private IUnitOfWork unitOfWork;

        public DataTypeService(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<DataType>> GetAsync()
        {
            return await Task.FromResult<IEnumerable<DataType>>(this.unitOfWork.DataTypes.GetPaged(0, int.MaxValue));
        }

        public async Task<DataType> GetByIdAsync(Guid Id)
        {
            return await this.unitOfWork.DataTypes.GetByIdAsync(Id);
        }
    }


}
