﻿using GraphQL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DynamicModel.Core.Web.Api.Graphql.Schema
{
    public class DataTypeSchema : GraphQL.Types.Schema
    {
        public DataTypeSchema(DataTypeQuery query, IDependencyResolver resolver)
        {
            Query = query;
            DependencyResolver = resolver;
        }
    }
}
