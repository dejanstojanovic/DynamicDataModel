﻿using DynamicModel.Core.Data.Entities.Graph;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DynamicModel.Core.Web.Api.Graphql.Schema
{
    public class DataTypeQuery: ObjectGraphType<object>
    {
        public DataTypeQuery(IService<DataType> dataTypes)
        {
            Name = "Query";
            Field<ListGraphType<DataTypeType>>(
                "dataTypes",
                resolve: context => dataTypes.GetAsync()
            );
        }
    }
}
