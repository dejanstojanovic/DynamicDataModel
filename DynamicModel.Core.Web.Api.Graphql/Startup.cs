﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DynamicModel.Core.Data.Entities;
using DynamicModel.Core.Data.Entities.Graph;
using DynamicModel.Core.Data.Repositories.UnitsOfWork;
using DynamicModel.Core.Web.Api.Graphql.Schema;
using GraphQL;
using GraphQL.Server.Transports.AspNetCore;
using GraphQL.Server.Transports.WebSockets;
using GraphQL.Types;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace DynamicModel.Core.Web.Api.Graphql
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        public IHostingEnvironment HostingEnvironment { get; }
        public Startup(IConfiguration configuration, IHostingEnvironment env)
        {
            Configuration = configuration;
            HostingEnvironment = env;
            //Log.Logger = new LoggerConfiguration()
            //                    .ReadFrom.Configuration(configuration)
            //                    .CreateLogger();
        }




        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<DynamicDbContext>(options =>
              options.UseSqlServer(Configuration.GetConnectionString("DynamicModelConnection")));

            services.AddScoped<IUnitOfWork, DynamicDataUnitOfWork>();


            services.AddSingleton<IDependencyResolver>(
                    c => new FuncDependencyResolver(type => c.GetRequiredService(type)));

            services.AddScoped<DataTypeQuery>();
            services.AddScoped<DataTypeSchema>();
            services.AddScoped<IService<DataType>, DataTypeService>();
            services.AddScoped<DataTypeType>();


            services.AddGraphQLHttp();
            services.AddGraphQLWebSocket<DataTypeSchema>();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseDefaultFiles();
            app.UseStaticFiles();
            app.UseWebSockets();
            app.UseGraphQLWebSocket<DataTypeSchema>(new GraphQLWebSocketsOptions());
            app.UseGraphQLHttp<DataTypeSchema>(new GraphQLHttpOptions());
        }
    }
}
