﻿using DynamicModel.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DynamicModel.Data.Repositories
{
    public interface IRepository<T> : IDisposable where T : BaseEntity
    {
        T GetById(Guid id);
        T GetById(String id);

        Task<T> GetByIdAsync(Guid id);
        Task<T> GetByIdAsync(String id);

        IEnumerable<T> GetPaged(int skip, int take);
        Task<IEnumerable<T>> GetPagedAsync(int skip, int take);

        void Insert(T item);
        void Update(T item);

        int Save();
        Task<int> SaveAsync();

        IQueryable<T> Query(Expression<Func<T, bool>> filter);
    }
}
