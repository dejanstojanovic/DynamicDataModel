﻿using DynamicModel.Data.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DynamicModel.Data.Repositories
{
    public abstract class BaseRepository<T> : IRepository<T> where T : BaseEntity
    {
        DynamicDbContext dbContext;
        bool disposing;

        public BaseRepository()
        {
            disposing = false;
            dbContext = new DynamicDbContext();

        }

        public virtual void Dispose()
        {
            if (!this.disposing)
            {
                this.dbContext.Dispose();
                this.disposing = true;
            }
        }

        public virtual T GetById(Guid id)
        {
            return this.dbContext.Set<T>().Find(id);
        }

        public virtual T GetById(string id)
        {
            return GetById(Guid.Parse(id));
        }

        public virtual async Task<T> GetByIdAsync(Guid id)
        {
            return await this.dbContext.Set<T>().FindAsync(id);
        }

        public virtual async Task<T> GetByIdAsync(string id)
        {
            return await GetByIdAsync(Guid.Parse(id));
        }

        public virtual IEnumerable<T> GetPaged(int skip, int take)
        {
            return this.dbContext.Set<T>().Skip(skip).Take(take);
        }

        public async Task<IEnumerable<T>> GetPagedAsync(int skip, int take)
        {
            return await this.dbContext.Set<T>().Skip(skip).Take(take).ToListAsync();
        }

        public virtual void Insert(T item)
        {
            item.Id = item.Id == null ? Guid.NewGuid() : item.Id;
            item.DateCreated = item.DateCreated == null || item.DateCreated == DateTime.MinValue ? DateTime.Now : item.DateCreated;

            this.dbContext.Set<T>().Add(item);
        }

        public virtual IQueryable<T> Query(System.Linq.Expressions.Expression<Func<T, bool>> filter)
        {
            return this.dbContext.Set<T>().Where(filter);
        }

        public virtual int Save()
        {
            return this.dbContext.SaveChanges();
        }

        public virtual async Task<int> SaveAsync()
        {
            return await this.dbContext.SaveChangesAsync();
        }

        public virtual void Update(T item)
        {
            item.DateModified = DateTime.Now;
            this.dbContext.Entry(item).State = EntityState.Modified;
        }
    }
}
