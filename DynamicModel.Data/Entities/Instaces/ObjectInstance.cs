﻿using DynamicModel.Data.Entities.Definitions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DynamicModel.Data.Entities.Instaces
{
    public class ObjectInstance : BaseEntity
    {
        [Required]
        [ForeignKey("Definition")]
        public Guid ObjectDefinitionId { get; set; }

        public ObjectDefintion Definition { get; set; }

        public IEnumerable<ObjectPropertyInstance> PropertyInstances { get; set; }
    }
}
