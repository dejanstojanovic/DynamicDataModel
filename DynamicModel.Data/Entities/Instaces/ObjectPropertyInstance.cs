﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DynamicModel.Data.Entities.Instaces
{
    public class ObjectPropertyInstance : BaseEntity
    {
        public ObjectInstance ObjectInstance { get; set; } 

        [Required]
        [ForeignKey("ObjectInstance")]
        public Guid ObjectInstanceId { get; set; }

        public String Value { get; set; }
    }
}
