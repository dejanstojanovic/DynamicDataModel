﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DynamicModel.Data.Entities
{
    public static class EntityFactory
    {
        public static T GetInstance<T>() where T : class, new()
        {
            var entity = new T() as BaseEntity;

            entity.Id = Guid.NewGuid();
            entity.DateCreated = DateTime.Now;
            entity.DateModified = null;

            return entity as T;

        }
    }
}
