﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DynamicModel.Data.Entities.Definitions
{
    public class ObjectDefintion:BaseEntity
    {
        [Required]
        [MaxLength(250)]
        [Index(IsUnique = true)]
        public String Name { get; set; }

        public IEnumerable<ObjectPropertyDefinition> PropertyDefinitions { get; set; }
    }
}
