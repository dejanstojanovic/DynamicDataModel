﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DynamicModel.Data.Entities.Definitions
{
    public class ObjectPropertyDataDefinition:BaseEntity
    {
        [Required]
        [MaxLength(100)]
        public String Name { get; set; }

        [Required]
        [MaxLength(250)]
        public String SystemType { get; set; }
    }
}
