﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DynamicModel.Data.Entities.Definitions
{
    public class ObjectPropertyDefinition:BaseEntity
    {
        public ObjectDefintion ObjectDefinition { get; set; }

        [Required]
        [ForeignKey("ObjectDefinition")]
        [Index("UniqueObjectPropertyDefintion", 2, IsUnique = true)]
        public Guid ObjectDefinitionId { get; set; }

        public ObjectPropertyDataDefinition DataDefinition { get; set; }

        [Required]
        [ForeignKey("DataDefinition")]
        public Guid DataDefinitionId { get; set; }


        [Required]
        [MaxLength(100)]
        [Index("UniqueObjectPropertyDefintion", 1, IsUnique = true)]
        public String Name { get; set; }

        public Boolean Required { get; set; }
    }
}
