﻿using DynamicModel.Data.Entities.Definitions;
using DynamicModel.Data.Entities.Instaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DynamicModel.Data.Entities
{
    class DynamicDbContext:DbContext
    {
        public DbSet<ObjectDefintion> ObjectsDefinitions { get; set; }
        public DbSet<ObjectPropertyDataDefinition> ObjectPropertyDataDefinitions { get; set; }
        public DbSet<ObjectPropertyDefinition> ObjectPropertyDefinitions { get; set; }
        public DbSet<ObjectInstance> ObjectInstances { get; set; }
        public DbSet<ObjectPropertyInstance> ObjectPropertyInstances { get; set; }
    }
}
