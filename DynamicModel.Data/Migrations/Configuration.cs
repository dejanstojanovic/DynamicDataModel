namespace DynamicModel.Data.Migrations
{
    using DynamicModel.Data.Entities;
    using DynamicModel.Data.Entities.Definitions;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<DynamicModel.Data.Entities.DynamicDbContext>
    {
        public Configuration()
        {
            //if (System.Diagnostics.Debugger.IsAttached == false)
            //{
            //    System.Diagnostics.Debugger.Launch();
            //}

            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(DynamicModel.Data.Entities.DynamicDbContext context)
        {
            List<ObjectPropertyDataDefinition> objectPropertyDataDefinitions = new List<ObjectPropertyDataDefinition>()
            {
                new ObjectPropertyDataDefinition(){Id=Guid.NewGuid(),DateCreated=DateTime.Now,Name=typeof(String).Name,SystemType= typeof(String).FullName},
                new ObjectPropertyDataDefinition(){Id=Guid.NewGuid(),DateCreated=DateTime.Now,Name="Integer",SystemType= typeof(int).FullName},
                new ObjectPropertyDataDefinition(){Id=Guid.NewGuid(),DateCreated=DateTime.Now,Name=typeof(Decimal).Name,SystemType= typeof(Decimal).FullName},
                new ObjectPropertyDataDefinition(){Id=Guid.NewGuid(),DateCreated=DateTime.Now,Name=typeof(Boolean).Name,SystemType= typeof(Boolean).FullName},
                new ObjectPropertyDataDefinition(){Id=Guid.NewGuid(),DateCreated=DateTime.Now,Name=typeof(DateTime).Name,SystemType= typeof(DateTime).FullName},
                new ObjectPropertyDataDefinition(){Id=Guid.NewGuid(),DateCreated=DateTime.Now,Name=typeof(Guid).Name.ToUpper(),SystemType= typeof(Guid).FullName},
                new ObjectPropertyDataDefinition(){Id=Guid.NewGuid(),DateCreated=DateTime.Now,Name="Strings",SystemType= typeof(String[]).FullName},
                new ObjectPropertyDataDefinition(){Id=Guid.NewGuid(),DateCreated=DateTime.Now,Name="Integers",SystemType= typeof(int[]).FullName}
            };
            objectPropertyDataDefinitions.ForEach(o => context.ObjectPropertyDataDefinitions.AddOrUpdate(p => p.Name, o));
            context.SaveChanges();


            //Get stored data definitions objects
            objectPropertyDataDefinitions = context.ObjectPropertyDataDefinitions.Select(d => d).ToList();

            var laptopObjestDefinition = new ObjectDefintion() { Id = Guid.NewGuid(), DateCreated = DateTime.Now, Name = "Laptop", Description = "Laptop item type" };
            context.ObjectsDefinitions.AddOrUpdate(p => p.Name, laptopObjestDefinition);
            context.SaveChanges();


            //Get stored object defintion
            laptopObjestDefinition = context.ObjectsDefinitions.Where(o => o.Name == laptopObjestDefinition.Name).FirstOrDefault();

            List<ObjectPropertyDefinition> objectPropertyDefinitions = new List<ObjectPropertyDefinition>()
            {
                new ObjectPropertyDefinition(){
                    Id =Guid.NewGuid(),
                    DateCreated =DateTime.Now,
                    Name ="Brand",
                    DataDefinition = objectPropertyDataDefinitions.First(d=>d.Name.Equals("String")),
                    ObjectDefinition = laptopObjestDefinition,
                    DataDefinitionId = objectPropertyDataDefinitions.First(d=>d.Name.Equals("String")).Id,
                    ObjectDefinitionId = laptopObjestDefinition.Id,
                    Required =true,
                    Description = "Brand name"
                    },
                new ObjectPropertyDefinition(){
                    Id =Guid.NewGuid(),
                    DateCreated =DateTime.Now,
                    Name ="ModelName",
                    DataDefinition = objectPropertyDataDefinitions.First(d=>d.Name.Equals("String")),
                    ObjectDefinition = laptopObjestDefinition,
                    DataDefinitionId = objectPropertyDataDefinitions.First(d=>d.Name.Equals("String")).Id,
                    ObjectDefinitionId = laptopObjestDefinition.Id,
                    Required =true,
                    Description = "Model name"
                    },
                new ObjectPropertyDefinition(){
                    Id =Guid.NewGuid(),
                    DateCreated =DateTime.Now,
                    Name ="ScreenSize",
                    DataDefinition = objectPropertyDataDefinitions.First(d=>d.Name.Equals("Integer")),
                    ObjectDefinition = laptopObjestDefinition,
                    DataDefinitionId = objectPropertyDataDefinitions.First(d=>d.Name.Equals("Integer")).Id,
                    ObjectDefinitionId = laptopObjestDefinition.Id,
                    Required =false,
                    Description = "Screen size in inch"
                    },
                new ObjectPropertyDefinition(){
                    Id =Guid.NewGuid(),
                    DateCreated =DateTime.Now,
                    Name ="HddSize",
                    DataDefinition = objectPropertyDataDefinitions.First(d=>d.Name.Equals("Integer")),
                    ObjectDefinition = laptopObjestDefinition,
                    DataDefinitionId = objectPropertyDataDefinitions.First(d=>d.Name.Equals("Integer")).Id,
                    ObjectDefinitionId = laptopObjestDefinition.Id,
                    Required =false,
                    Description = "Hard drive size in GB"
                    },
                 new ObjectPropertyDefinition(){
                    Id =Guid.NewGuid(),
                    DateCreated =DateTime.Now,
                    Name ="Price",
                    DataDefinition = objectPropertyDataDefinitions.First(d=>d.Name.Equals("Decimal")),
                    ObjectDefinition = laptopObjestDefinition,
                    DataDefinitionId = objectPropertyDataDefinitions.First(d=>d.Name.Equals("Decimal")).Id,
                    ObjectDefinitionId = laptopObjestDefinition.Id,
                    Required =false,
                    Description = "Laptop price"
                    },
                 new ObjectPropertyDefinition(){
                    Id =Guid.NewGuid(),
                    DateCreated =DateTime.Now,
                    Name ="InStock",
                    DataDefinition = objectPropertyDataDefinitions.First(d=>d.Name.Equals("Boolean")),
                    ObjectDefinition = laptopObjestDefinition,
                    DataDefinitionId = objectPropertyDataDefinitions.First(d=>d.Name.Equals("Boolean")).Id,
                    ObjectDefinitionId = laptopObjestDefinition.Id,
                    Required =true,
                    Description = "Is item available in stock"
                    },
            };
            objectPropertyDefinitions.ForEach(o => context.ObjectPropertyDefinitions.AddOrUpdate(p => p.Name, o));

            context.SaveChanges();
        }
    }
}
