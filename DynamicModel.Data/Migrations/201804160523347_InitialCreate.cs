namespace DynamicModel.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ObjectInstances",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        ObjectDefinitionId = c.Guid(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        DateModified = c.DateTime(),
                        Description = c.String(maxLength: 250),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ObjectDefintions", t => t.ObjectDefinitionId, cascadeDelete: true)
                .Index(t => t.ObjectDefinitionId);
            
            CreateTable(
                "dbo.ObjectDefintions",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 250),
                        DateCreated = c.DateTime(nullable: false),
                        DateModified = c.DateTime(),
                        Description = c.String(maxLength: 250),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true);
            
            CreateTable(
                "dbo.ObjectPropertyDataDefinitions",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        SystemType = c.String(nullable: false, maxLength: 250),
                        DateCreated = c.DateTime(nullable: false),
                        DateModified = c.DateTime(),
                        Description = c.String(maxLength: 250),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ObjectPropertyDefinitions",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        ObjectDefinitionId = c.Guid(nullable: false),
                        DataDefinitionId = c.Guid(nullable: false),
                        Name = c.String(nullable: false, maxLength: 100),
                        Required = c.Boolean(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        DateModified = c.DateTime(),
                        Description = c.String(maxLength: 250),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ObjectPropertyDataDefinitions", t => t.DataDefinitionId, cascadeDelete: true)
                .ForeignKey("dbo.ObjectDefintions", t => t.ObjectDefinitionId, cascadeDelete: true)
                .Index(t => new { t.Name, t.ObjectDefinitionId }, unique: true, name: "UniqueObjectPropertyDefintion")
                .Index(t => t.DataDefinitionId);
            
            CreateTable(
                "dbo.ObjectPropertyInstances",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        ObjectInstanceId = c.Guid(nullable: false),
                        Value = c.String(),
                        DateCreated = c.DateTime(nullable: false),
                        DateModified = c.DateTime(),
                        Description = c.String(maxLength: 250),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ObjectInstances", t => t.ObjectInstanceId, cascadeDelete: true)
                .Index(t => t.ObjectInstanceId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ObjectPropertyInstances", "ObjectInstanceId", "dbo.ObjectInstances");
            DropForeignKey("dbo.ObjectPropertyDefinitions", "ObjectDefinitionId", "dbo.ObjectDefintions");
            DropForeignKey("dbo.ObjectPropertyDefinitions", "DataDefinitionId", "dbo.ObjectPropertyDataDefinitions");
            DropForeignKey("dbo.ObjectInstances", "ObjectDefinitionId", "dbo.ObjectDefintions");
            DropIndex("dbo.ObjectPropertyInstances", new[] { "ObjectInstanceId" });
            DropIndex("dbo.ObjectPropertyDefinitions", new[] { "DataDefinitionId" });
            DropIndex("dbo.ObjectPropertyDefinitions", "UniqueObjectPropertyDefintion");
            DropIndex("dbo.ObjectDefintions", new[] { "Name" });
            DropIndex("dbo.ObjectInstances", new[] { "ObjectDefinitionId" });
            DropTable("dbo.ObjectPropertyInstances");
            DropTable("dbo.ObjectPropertyDefinitions");
            DropTable("dbo.ObjectPropertyDataDefinitions");
            DropTable("dbo.ObjectDefintions");
            DropTable("dbo.ObjectInstances");
        }
    }
}
