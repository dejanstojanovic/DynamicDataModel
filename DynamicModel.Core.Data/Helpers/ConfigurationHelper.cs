﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace DynamicModel.Core.Data.Helpers
{
    internal static class ConfigurationHelper
    {
        public static IConfigurationBuilder GetDefaultConfiguration()
        {
            return new ConfigurationBuilder()
                .AddJsonFile(Path.Combine(Directory.GetCurrentDirectory(), $"appsettings.json"));
        }

    }
}
