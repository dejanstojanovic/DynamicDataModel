﻿using DynamicModel.Core.Data.Migrations;
using Microsoft.EntityFrameworkCore.Design;
using DynamicModel.Core.Data.Migrations.Seeding;
using DynamicModel.Core.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace DynamicModel.Core.Data
{
    public class DynamicModelDbContextFactory : IDesignTimeDbContextFactory<DynamicDbContext>
    {
        public DynamicDbContext CreateDbContext(string[] args)
        {
            var dbContext = new DynamicDbContext(new DbContextOptionsBuilder<DynamicDbContext>().UseSqlServer(
                new ConfigurationBuilder()
                    .AddJsonFile(Path.Combine(Directory.GetCurrentDirectory(), $"appsettings.json"))
                    .Build()
                    .GetConnectionString("DynamicModelConnection")
                ).Options);

            dbContext.Database.Migrate();

            new DataSeeder(dbContext).Seed();
            return dbContext;
        }
    }
}
