﻿using DynamicModel.Core.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DynamicModel.Core.Data.Exceptions
{
    public class EntityReferencedException:Exception
    {
        BaseEntity referenced;
        IEnumerable<BaseEntity> referencingFrom;
        public EntityReferencedException(BaseEntity referenced, IEnumerable<BaseEntity> referencingFrom)
        {
            this.referenced = referenced;
            this.referencingFrom = referencingFrom;
        }

        public override string Message => $"Entity id:{referenced.Id} of type: {referenced.GetType().Name} is used by entities id: {String.Concat(referencingFrom.Select(r=>r.Id),",")} of type {referencingFrom.First().GetType().Name}";
    }
}
