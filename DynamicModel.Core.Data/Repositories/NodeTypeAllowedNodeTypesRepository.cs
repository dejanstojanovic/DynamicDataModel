﻿using DynamicModel.Core.Data.Entities;
using DynamicModel.Core.Data.Entities.Graph;
using Microsoft.Extensions.Configuration;

namespace DynamicModel.Core.Data.Repositories
{
    public class NodeTypeAllowedNodeTypesRepository : BaseRepository<NodeTypeAllowedNodeType>
    {
        public NodeTypeAllowedNodeTypesRepository(DynamicDbContext dbContext) : base(dbContext)
        {
        }
    }
}
