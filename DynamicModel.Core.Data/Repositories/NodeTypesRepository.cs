﻿using Microsoft.Extensions.Configuration;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Linq.Expressions;
using DynamicModel.Core.Data.Repositories;
using DynamicModel.Core.Data.Entities;
using DynamicModel.Core.Data.Entities.Graph;

namespace DynamicModel.Core.Data.Repositories
{
    public class NodeTypesRepository : BaseRepository<NodeType>
    {
        public NodeTypesRepository(DynamicDbContext context) : base(context)
        {
        }

        public override IQueryable<NodeType> Find(Expression<Func<NodeType, bool>> expression)
        {
            return base.Find(expression).Include(o=>o.PropertyTypes).ThenInclude(p=>p.DataType);
        }

        public override IQueryable<NodeType> GetPaged(int skip, int take)
        {
            return base.GetPaged(skip, take);
            //    .Include(o=>o.NodeTypeAllowedNodeTypes)
            //    .Include(o => o.PropertyTypes)
            //    .ThenInclude(p => p.DataType);
        }

    }
}
