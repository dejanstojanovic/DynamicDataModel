﻿using DynamicModel.Core.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DynamicModel.Core.Data.Repositories
{
    public abstract class BaseRepository<T> : IRepository<T> where T : BaseEntity
    {

        DynamicDbContext dbContext;
        bool disposing;

        public DynamicDbContext DbContext
        {
            get
            {
                return this.dbContext;
            }
        }

        public BaseRepository(DynamicDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public virtual void Dispose()
        {
            if (!this.disposing)
            {
                this.dbContext.Dispose();
                this.disposing = true;
            }
        }



        public virtual async Task<T> GetByIdAsync(Guid id)
        {
            return await this.dbContext.Set<T>().FindAsync(id);
        }

        public virtual async Task<T> GetByIdAsync(string id)
        {
            return await GetByIdAsync(Guid.Parse(id));
        }

        public virtual IQueryable<T> GetPaged(int skip, int take)
        {
            return this.dbContext.Set<T>().AsNoTracking().Skip(skip).Take(take);
        }

        public virtual async Task InsertAsync(T item)
        {
            item.Id = item.Id == null ? Guid.NewGuid() : item.Id;
            item.DateCreated = item.DateCreated == null || item.DateCreated == DateTime.MinValue ? DateTime.Now : item.DateCreated;

           await this.dbContext.Set<T>().AddAsync(item);
        }

        public virtual IQueryable<T> Find(System.Linq.Expressions.Expression<Func<T, bool>> expression)
        {
            return this.dbContext.Set<T>().AsNoTracking().Where(expression);
        }

        public void Remove(Guid id)
        {
            var item = this.dbContext.Set<T>().FirstOrDefault(e => e.Id == id);
            if (item != null)
            {
                this.dbContext.Set<T>().Remove(item);
            }
        }

        public void RemoveRange(IEnumerable<Guid> ids)
        {
            var items = this.dbContext.Set<T>().Where(e => ids.Contains(e.Id));
            if (items != null && items.Any())
            {
                this.dbContext.Set<T>().RemoveRange(items);
            }
        }

        public async Task InsertRangeAsync(IEnumerable<T> items)
        {
             await this.dbContext.Set<T>().AddRangeAsync(items);
        }


    }
}
