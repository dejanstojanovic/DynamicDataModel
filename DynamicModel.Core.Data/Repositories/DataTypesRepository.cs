﻿using DynamicModel.Core.Data.Entities;
using DynamicModel.Core.Data.Entities.Graph;
using Microsoft.Extensions.Configuration;

namespace DynamicModel.Core.Data.Repositories
{
    public class DataTypesRepository : BaseRepository<DataType>
    {
        public DataTypesRepository(DynamicDbContext dbContext) : base(dbContext)
        {
        }
    }
}
