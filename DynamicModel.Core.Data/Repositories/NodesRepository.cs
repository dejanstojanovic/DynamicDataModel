﻿using DynamicModel.Core.Data.Entities;
using Microsoft.Extensions.Configuration;
using System;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using DynamicModel.Core.Data.Entities.Graph;
using System.Threading.Tasks;

namespace DynamicModel.Core.Data.Repositories
{
    public class NodesRepository : BaseRepository<Node>
    {
        public NodesRepository(DynamicDbContext dbContext) : base(dbContext)
        {
        }

        public override async Task<Node> GetByIdAsync(Guid id)
        {
            return await base.Find(n=>n.Id.Equals(id))
                .Include(n=>n.Properties)
                .Include(n=>n.Parent)
                .Include(n=>n.Children)
                .FirstOrDefaultAsync();
        }


        public override IQueryable<Node> Find(Expression<Func<Node, bool>> expression)
        {
            return base.Find(expression)
                .Include(n => n.Properties)
                .Include(n => n.Parent)
                .Include(n => n.Children);
        }

        public override IQueryable<Node> GetPaged(int skip, int take)
        {
            return base.GetPaged(skip, take)
                .Include(n => n.Properties)
                .Include(n => n.Parent)
                .Include(n => n.Children);
        }
    }
}
