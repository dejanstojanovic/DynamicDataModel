﻿using DynamicModel.Core.Data.Entities;
using DynamicModel.Core.Data.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DynamicModel.Core.Data.Repositories.UnitsOfWork
{
    public interface IUnitOfWork : IDisposable
    {
        NodeTypesRepository NodeTypes { get; }
        DataTypesRepository DataTypes { get; }
        PropertyTypesRepository PropertyTypes { get; }
        NodeTypeAllowedNodeTypesRepository NodeTypeAllowedNodeTypes { get; }

        NodesRepository Nodes { get; }
        PropertiesRepository Properties { get; }

        void Update<T>(T entity) where T : BaseEntity;

        int Save();
        Task<int> SaveAsync();

        IDatabaseTransaction BeginTransaction { get; }
    }
}
