﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DynamicModel.Core.Data.Repositories.UnitsOfWork
{
    public interface IDatabaseTransaction : IDisposable
    {
        void Commit();

        void Rollback();
    }
}
