﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DynamicModel.Core.Data.Entities;
using DynamicModel.Core.Data.Repositories;
using Microsoft.Extensions.Configuration;

namespace DynamicModel.Core.Data.Repositories.UnitsOfWork
{
    public class DynamicDataUnitOfWork : IUnitOfWork
    {
        #region Fields
        DynamicDbContext dbContext;
        bool disposing;
        #endregion

        #region Properties
        public NodeTypesRepository NodeTypes { get; private set; }
        public NodeTypeAllowedNodeTypesRepository NodeTypeAllowedNodeTypes { get; private set; }
        public DataTypesRepository DataTypes { get; private set; }
        public PropertyTypesRepository PropertyTypes { get; private set; }
        public NodesRepository Nodes { get; private set; }
        public PropertiesRepository Properties { get; private set; }
        #endregion


        public DynamicDataUnitOfWork(DynamicDbContext dbContext)
        {
            this.dbContext = dbContext;
            disposing = false;

            this.NodeTypes = new NodeTypesRepository(dbContext);
            this.DataTypes = new DataTypesRepository(dbContext);
            this.PropertyTypes = new PropertyTypesRepository(dbContext);
            this.NodeTypeAllowedNodeTypes = new NodeTypeAllowedNodeTypesRepository(dbContext);

            this.Nodes = new NodesRepository(dbContext);
            this.Properties = new PropertiesRepository(dbContext);
        }

        public IDatabaseTransaction BeginTransaction
        {
            get
            {
                return new EntityDatabaseTransaction(this.dbContext);
            }
        }

        public void Dispose()
        {
            if (!this.disposing)
            {
                this.dbContext.Dispose();
                this.disposing = true;
            }
        }

        public void Update<T>(T entity) where T : BaseEntity
        {
            this.dbContext.Set<T>().Update(entity);
        }

        public int Save()
        {
            return this.dbContext.SaveChanges();
        }

        public Task<int> SaveAsync()
        {
            return this.dbContext.SaveChangesAsync();
        }
    }
}
