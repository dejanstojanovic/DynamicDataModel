﻿using DynamicModel.Core.Data.Entities;
using DynamicModel.Core.Data.Entities.Graph;
using Microsoft.Extensions.Configuration;

namespace DynamicModel.Core.Data.Repositories
{
    public class PropertiesRepository : BaseRepository<Property>
    {
        public PropertiesRepository(DynamicDbContext dbContext) : base(dbContext)
        {
        }
    }
}
