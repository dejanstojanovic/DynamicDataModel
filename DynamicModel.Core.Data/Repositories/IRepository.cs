﻿using DynamicModel.Core.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DynamicModel.Core.Data.Repositories
{
    public interface IRepository<T> : IDisposable where T : BaseEntity
    {
        //T GetById(Guid id);
        //T GetById(String id);

        Task<T> GetByIdAsync(Guid id);
        Task<T> GetByIdAsync(String id);

        IQueryable<T> GetPaged(int skip, int take);
        IQueryable<T> Find(Expression<Func<T, bool>> expression);

        Task InsertAsync(T item);
        Task InsertRangeAsync(IEnumerable<T> items);

        void Remove(Guid id);
        void RemoveRange(IEnumerable<Guid> ids);


        
    }
}
