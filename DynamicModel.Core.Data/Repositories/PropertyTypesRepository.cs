﻿using DynamicModel.Core.Data.Entities;
using DynamicModel.Core.Data.Entities.Graph;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace DynamicModel.Core.Data.Repositories
{
    public class PropertyTypesRepository : BaseRepository<PropertyType>
    {
        public PropertyTypesRepository(DynamicDbContext dbContext) : base(dbContext)
        {
        }

        public override IQueryable<PropertyType> Find(Expression<Func<PropertyType, bool>> expression)
        {
            return base.Find(expression).Include(o => o.NodeType);
        }

        public override IQueryable<PropertyType> GetPaged(int skip, int take)
        {
            return base.GetPaged(skip, take).Include(o => o.NodeType);
        }
    }
}
