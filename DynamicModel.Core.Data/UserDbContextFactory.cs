﻿using DynamicModel.Core.Data.Migrations;
using Microsoft.EntityFrameworkCore.Design;
using DynamicModel.Core.Data.Migrations.Seeding;
using DynamicModel.Core.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace DynamicModel.Core.Data
{
    public class UserDbContextFactory : IDesignTimeDbContextFactory<UserDbContext>
    {
        public UserDbContext CreateDbContext(string[] args)
        {
            var dbContext = new UserDbContext(new DbContextOptionsBuilder<UserDbContext>().UseSqlServer(
                new ConfigurationBuilder()
                    .AddJsonFile(Path.Combine(Directory.GetCurrentDirectory(), $"appsettings.json"))
                    .Build()
                    .GetConnectionString("DynamicModelConnection")
                ).Options);


            return dbContext;
        }
    }
}
