﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DynamicModel.Core.Data.Entities.Graph
{
    public class Property : BaseEntity
    {

        [Required]
        [Column(Order = 1)]
        public Guid PropertyTypeId { get; set; }
        public PropertyType PropertyType { get; set; }

        [Required]
        [Column(Order = 2)]
        public Guid NodeId { get; set; }
        public Node Node { get; set; }

        [Column(Order = 3)]
        public String Value { get; set; }
    }


    internal class PropertyConfiguration : BaseEntityConfiguration<Property>
    {
        public override void Configure(EntityTypeBuilder<Property> builder)
        {
            base.Configure(builder);
            builder.HasOne<PropertyType>(p => p.PropertyType).WithMany().OnDelete(DeleteBehavior.Restrict);
            builder.HasOne<Node>(p => p.Node);
        }
    }


}
