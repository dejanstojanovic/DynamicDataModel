﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DynamicModel.Core.Data.Entities.Graph
{
    public class NodeTypeAllowedNodeType : BaseEntity
    {

        [Required]
        [Column(Order = 1)]
        public Guid NodeTypeId { get; set; }
        public NodeType NodeType { get; set; }

        [Required]
        [Column(Order = 2)]
        public Guid AllowedNodeTypeId { get; set; }
        public NodeType AllowedNodeType { get; set; }

        [Required]
        [Column(Order = 3)]
        public Boolean MultipleOccurences { get; set; }


    }

    internal class NodeTypeAllowedNodeTypeConfiguration : BaseEntityConfiguration<NodeTypeAllowedNodeType>
    {
        public override void Configure(EntityTypeBuilder<NodeTypeAllowedNodeType> builder)
        {
            base.Configure(builder);
            builder.Ignore(p => p.Id);
            builder.HasKey(t => new { t.NodeTypeId, t.AllowedNodeTypeId });
            builder.HasOne(t => t.NodeType).WithMany(t => t.NodeTypeAllowedNodeTypes).OnDelete(DeleteBehavior.Restrict);
            builder.HasOne(t => t.AllowedNodeType).WithMany().OnDelete(DeleteBehavior.Restrict);
            builder.Property(p => p.MultipleOccurences).HasDefaultValue(true);
        }

    }
}
