﻿using System;
using System.Collections.Generic;

namespace DynamicModel.Core.Data.Entities.Graph
{
    public class AllowedChildrenNodeType: BaseEntity
    {
        public Guid NodeTypeId { get; set; }
        public NodeType NodeType { get; set; }


        public IEnumerable<Guid> AllowedChildernNodeTypeIds { get; set; }
        public IEnumerable<NodeType> AllowedChildrenNodeTypes { get; set; }

    }
}
