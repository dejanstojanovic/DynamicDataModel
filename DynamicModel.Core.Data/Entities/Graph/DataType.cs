﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DynamicModel.Core.Data.Entities.Graph
{
    public class DataType:BaseEntity
    {
        [Required]
        [MaxLength(100)]
        [Column(Order = 1)]
        public String Name { get; set; }

        [Required]
        [MaxLength(250)]
        [Column(Order = 2)]
        public String SystemType { get; set; }
    }

    internal class DataTypenConfiguration : BaseEntityConfiguration<DataType>
    {
        public override void Configure(EntityTypeBuilder<DataType> builder)
        {
            base.Configure(builder);
            builder.HasIndex(p => p.Name).IsUnique();
        }
    }
}
