﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DynamicModel.Core.Data.Entities.Graph
{
    public class Node : BaseEntity
    {
        [Required]
        [Column(Order = 1)]
        public Guid NodeTypeId { get; set; }
        public NodeType NodeType { get; set; }

        [Required]
        [MaxLength(250)]
        [Column(Order = 2)]
        public String Name { get; set; }

        [Column(Order = 3)]
        public Guid? ParentId { get; set; }
        public Node Parent { get; set; }

        public IEnumerable<Property> Properties { get; set; }
        public IEnumerable<Node> Children { get; set; }

    }

    internal class NodeConfiguration : BaseEntityConfiguration<Node>
    {
        public override void Configure(EntityTypeBuilder<Node> builder)
        {
            base.Configure(builder);
            builder.HasIndex(p => new { p.Name, p.ParentId }).IsUnique(true);
            builder.HasOne<NodeType>(p => p.NodeType);
            builder.HasOne<Node>(p => p.Parent);
            builder.HasMany<Node>(o => o.Children).WithOne(p => p.Parent).OnDelete(DeleteBehavior.Restrict);
            builder.HasMany<Property>(o => o.Properties).WithOne(o => o.Node);
        }

    }
}
