﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace DynamicModel.Core.Data.Entities.Graph
{
    public class NodeType : BaseEntity
    {
        [Required]
        [MaxLength(250)]
        [Column(Order = 1)]
        public String Name { get; set; }

        [Column(Order = 4)]
        public Guid? InheritedFromNodeTypeId { get; set; }
        public NodeType InheritedFromNodeType { get; set; }

        [Required]
        [Column(Order = 5)]
        public Boolean CanBeRoot { get; set; }

        public IEnumerable<PropertyType> PropertyTypes { get; set; }

        public ICollection<NodeTypeAllowedNodeType> NodeTypeAllowedNodeTypes { get; } = new List<NodeTypeAllowedNodeType>();

    }

    internal class NodeTypeConfiguration : BaseEntityConfiguration<NodeType>
    {
        public override void Configure(EntityTypeBuilder<NodeType> builder)
        {
            base.Configure(builder);
            builder.HasIndex(p => p.Name).IsUnique();
            builder.Property(p => p.CanBeRoot).HasDefaultValue(true);
            builder.HasOne(p => p.InheritedFromNodeType);

            builder.HasMany(p => p.PropertyTypes);
            builder.HasMany(p => p.NodeTypeAllowedNodeTypes);
        }
    }
}
