﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DynamicModel.Core.Data.Entities.Graph
{
    public class PropertyType : BaseEntity
    {
        [Required]
        [MaxLength(100)]
        [Column(Order = 1)]
        public String Name { get; set; }

        [Required]
        [Column(Order = 2)]
        public Guid NodeTypeId { get; set; }
        public NodeType NodeType { get; set; }

        [Required]
        [Column(Order = 3)]
        public Guid DataTypeId { get; set; }
        public DataType DataType { get; set; }

        [Column(Order = 4)]
        public Boolean Required { get; set; }
    }

    internal class PropertyTypeConfiguration : BaseEntityConfiguration<PropertyType>
    {
        public override void Configure(EntityTypeBuilder<PropertyType> builder)
        {
            base.Configure(builder);
            builder.HasIndex(t => new { t.Name, t.NodeTypeId }).IsUnique(true);
            builder.HasOne(p => p.DataType);
            builder.HasOne(p => p.NodeType);
        }
    }
}
