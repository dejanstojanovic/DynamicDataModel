﻿using DynamicModel.Core.Data.Entities.Graph;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;

namespace DynamicModel.Core.Data.Entities
{
    public class DynamicDbContext : DbContext
    {

        private readonly IConfigurationRoot configuration;
        private readonly IMemoryCache memoryCache;

        public DynamicDbContext(DbContextOptions<DynamicDbContext> options, IConfigurationRoot config, IMemoryCache memCache)
         : base(options)
        {
            configuration = config;
            memoryCache = memCache;
        }

        public DynamicDbContext(DbContextOptions<DynamicDbContext> options) : base(options)
        {
            
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {

            base.OnModelCreating(builder);

            //TODO: Load configurations from namepsace with reflection with filter on internals which inherit BaseEntityConfiguration

            //Types configurations
            builder.ApplyConfiguration(new NodeTypeConfiguration());
            builder.ApplyConfiguration(new PropertyTypeConfiguration());
            builder.ApplyConfiguration(new DataTypenConfiguration());
            builder.ApplyConfiguration(new NodeTypeAllowedNodeTypeConfiguration());


            //Istance configurations
            builder.ApplyConfiguration(new NodeConfiguration());
            builder.ApplyConfiguration(new PropertyConfiguration());

           
        }

        public DbSet<NodeType> NodeTypes { get; set; }
        public DbSet<NodeTypeAllowedNodeType> NodeTypeAllowedNodeTypes { get; set; }
        public DbSet<DataType> DataTypes { get; set; }
        public DbSet<PropertyType> PropertyTypes { get; set; }
        public DbSet<Node> Nodes { get; set; }
        public DbSet<Property> Properties { get; set; }

    }
}
