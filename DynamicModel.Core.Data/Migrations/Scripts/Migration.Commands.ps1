﻿Add-Migration InitialCreate -Project DynamicModel.Core.Data  -Context DynamicDbContext
Update-Database -Project DynamicModel.Core.Data -Context DynamicDbContext
Remove-Migration -Project DynamicModel.Core.Data -Context DynamicDbContext


Add-Migration InitialCreate -Project DynamicModel.Core.Data -Context UserDbContext
Update-Database -Project DynamicModel.Core.Data -Context UserDbContext
Remove-Migration -Project DynamicModel.Core.Data -Context UserDbContext

