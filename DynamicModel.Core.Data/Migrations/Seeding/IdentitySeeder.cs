﻿using DynamicModel.Core.Data.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DynamicModel.Core.Data.Migrations.Seeding
{
    public class IdentitySeeder
    {
        private UserDbContext context;

        public IdentitySeeder(UserDbContext context)
        {
            this.context = context;
        }

        public void Seed()
        {


            if (!context.Users.Any())
            {
                //var roleStore = new RoleStore<IdentityRole>(context);
                //var roleManager = new RoleManager<IdentityRole>(roleStore);
                //var userStore = new UserStore<IdentityUser>(context);
                //var userManager = new UserManager<IdentityUser>(userStore);

                //// Add missing roles
                //var role = roleManager.FindByName("Admin");
                //if (role == null)
                //{
                //    role = new IdentityRole("Admin");
                //    roleManager.Create(role);
                //}

                //// Create test users
                //var user = userManager.FindByName("admin");
                //if (user == null)
                //{
                //    var newUser = new ApplicationUser()
                //    {
                //        UserName = "admin",
                //        FirstName = "Admin",
                //        LastName = "User",
                //        Email = "xxx@xxx.net",
                //        PhoneNumber = "5551234567",
                //        MustChangePassword = false
                //    };
                //    userManager.Create(newUser, "Password1");
                //    userManager.SetLockoutEnabled(newUser.Id, false);
                //    userManager.AddToRole(newUser.Id, "Admin");
                }
            }


        
    }
}
