﻿using DynamicModel.Core.Data.Entities;
using DynamicModel.Core.Data.Entities.Graph;
using DynamicModel.Core.Data.Extensions;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace DynamicModel.Core.Data.Migrations.Seeding
{
    internal class DataSeeder
    {
        private DynamicDbContext context;

        public DataSeeder(DynamicDbContext context)
        {
            this.context = context;
        }

        public void Seed()
        {
            context.DataTypes.RemoveRange(context.DataTypes.ToArray());
            context.PropertyTypes.RemoveRange(context.PropertyTypes.ToArray());
            context.NodeTypes.RemoveRange(context.NodeTypes.ToArray());
            context.NodeTypeAllowedNodeTypes.RemoveRange(context.NodeTypeAllowedNodeTypes.ToArray());
            context.Nodes.RemoveRange(context.Nodes.ToArray());
            context.Properties.RemoveRange(context.Properties.ToArray());

            context.SaveChanges();

            SeedDataTypes();
        }


        private void SeedDataTypes()
        {

            List<DataType> dataTypes = new List<DataType>()
            {
                new DataType(){Id=Guid.NewGuid(),DateCreated=DateTime.Now,Name=typeof(String).Name,SystemType= typeof(String).FullName},
                new DataType(){Id=Guid.NewGuid(),DateCreated=DateTime.Now,Name="Integer",SystemType= typeof(int).FullName},
                new DataType(){Id=Guid.NewGuid(),DateCreated=DateTime.Now,Name=typeof(Decimal).Name,SystemType= typeof(Decimal).FullName},
                new DataType(){Id=Guid.NewGuid(),DateCreated=DateTime.Now,Name=typeof(Boolean).Name,SystemType= typeof(Boolean).FullName},
                new DataType(){Id=Guid.NewGuid(),DateCreated=DateTime.Now,Name=typeof(DateTime).Name,SystemType= typeof(DateTime).FullName},
                new DataType(){Id=Guid.NewGuid(),DateCreated=DateTime.Now,Name=typeof(Guid).Name.ToUpper(),SystemType= typeof(Guid).FullName},
                new DataType(){Id=Guid.NewGuid(),DateCreated=DateTime.Now,Name="Strings",SystemType= typeof(String[]).FullName},
                new DataType(){Id=Guid.NewGuid(),DateCreated=DateTime.Now,Name="Integers",SystemType= typeof(int[]).FullName}
            };
            dataTypes.ForEach(o => context.DataTypes.AddOrUpdate(o, p => p.Name));
            context.SaveChanges();

            SeedNodeTypes();
        }

        private void SeedNodeTypes()
        {
            //Get stored data definitions objects
            var dataTypes = context.DataTypes.Select(d => d).ToList();

            #region Organization node type
            var organizationNodeType = new NodeType() { Id = Guid.NewGuid(), CanBeRoot = true, DateCreated = DateTime.Now, Name = "Organization", Description = "Organization entity" };
            context.NodeTypes.AddOrUpdate(organizationNodeType, p => p.Name);


            var companyAreaProperyType = new PropertyType()
            {
                Id = Guid.NewGuid(),
                Name = "Area",
                DataTypeId = dataTypes.First(d => d.Name.Equals("String")).Id,
                NodeTypeId = organizationNodeType.Id,
                Required = true,
                Description = "Company geo area"
            };

            var companyDesignationPropertyType = new PropertyType()
            {
                Id = Guid.NewGuid(),
                Name = "Designations",
                DataTypeId = dataTypes.First(d => d.Name.Equals("Strings")).Id,
                NodeTypeId = organizationNodeType.Id,
                Required = true,
                Description = "Company designations"
            };

            var companyGovernmentPropertyType = new PropertyType()
            {
                Id = Guid.NewGuid(),
                Name = "Government",
                DataTypeId = dataTypes.First(d => d.Name.Equals("Boolean")).Id,
                NodeTypeId = organizationNodeType.Id,
                Required = true,
                Description = "Is government company"
            };

            List<PropertyType> organizationPropertyTypes = new List<PropertyType>()
            {
                companyAreaProperyType,
                companyDesignationPropertyType,
                companyGovernmentPropertyType
            };

            context.PropertyTypes.AddOrUpdate(organizationPropertyTypes, p => p.NodeTypeId, p => p.Name);

            Node companyNode = new Node()
            {
                Id = Guid.NewGuid(),
                Name = "Northwind",
                NodeTypeId = organizationNodeType.Id,
                ParentId = null
            };

            context.Properties.AddOrUpdate(new Property()
            {
                Id = Guid.NewGuid(),
                NodeId = companyNode.Id,
                PropertyTypeId = companyAreaProperyType.Id,
                Value = Newtonsoft.Json.JsonConvert.SerializeObject("USA")
            }, p => p.NodeId, p => p.PropertyTypeId);

            context.Properties.AddOrUpdate(new Property()
            {
                Id = Guid.NewGuid(),
                NodeId = companyNode.Id,
                PropertyTypeId = companyDesignationPropertyType.Id,
                Value = Newtonsoft.Json.JsonConvert.SerializeObject("Trading & Sales")
            }, p => p.NodeId, p => p.PropertyTypeId);

            context.Properties.AddOrUpdate(new Property()
            {
                Id = Guid.NewGuid(),
                NodeId = companyNode.Id,
                PropertyTypeId = companyGovernmentPropertyType.Id,
                Value = Newtonsoft.Json.JsonConvert.SerializeObject(true)
            }, p => p.NodeId, p => p.PropertyTypeId);


            context.Nodes.AddOrUpdate(companyNode, p => p.Name, p => p.ParentId);

            #endregion

            #region Employee node type
            var employeeNodeType = new NodeType()
            {
                Id = Guid.NewGuid(),
                CanBeRoot = false,
                DateCreated = DateTime.Now,
                Name = "Employee",
                Description = "Employee entity"
            }
            ;
            context.NodeTypes.AddOrUpdate(employeeNodeType, p => p.Name);

            var employeeFirstNamePropertyType = new PropertyType()
            {
                Id = Guid.NewGuid(),
                Name = "FirstName",
                DataTypeId = dataTypes.First(d => d.Name.Equals("String")).Id,
                NodeTypeId = employeeNodeType.Id,
                Required = true,
                Description = "First name"
            };

            var emaployeeLastNamePropertyType = new PropertyType()
            {
                Id = Guid.NewGuid(),
                Name = "LastName",
                DataTypeId = dataTypes.First(d => d.Name.Equals("String")).Id,
                NodeTypeId = employeeNodeType.Id,
                Required = true,
                Description = "Last name"
            };

            var employeeTitlePropertyType = new PropertyType()
            {
                Id = Guid.NewGuid(),
                Name = "Title",
                DataTypeId = dataTypes.First(d => d.Name.Equals("String")).Id,
                NodeTypeId = employeeNodeType.Id,
                Required = true,
                Description = "Job title"
            };

            var employeeExtensionPropertyType = new PropertyType()
            {
                Id = Guid.NewGuid(),
                Name = "Extension",
                DataTypeId = dataTypes.First(d => d.Name.Equals("Integer")).Id,
                NodeTypeId = employeeNodeType.Id,
                Required = true,
                Description = "Phone extension"
            };

            var employeeDobPropertyType = new PropertyType()
            {
                Id = Guid.NewGuid(),
                Name = "DOB",
                DataTypeId = dataTypes.First(d => d.Name.Equals("DateTime")).Id,
                NodeTypeId = employeeNodeType.Id,
                Required = true,
                Description = "Date of birth"
            };

            List<PropertyType> employeePropertyTypes = new List<PropertyType>()
            {
                //employeeIdPropertyType,
                employeeFirstNamePropertyType,
                emaployeeLastNamePropertyType,
                employeeTitlePropertyType,
                employeeExtensionPropertyType,
                employeeDobPropertyType
            };

            context.PropertyTypes.AddOrUpdate(employeePropertyTypes, p => p.NodeTypeId, p => p.Name);

            #region Employee 1
            Node employeePresident = new Node()
            {
                Id = Guid.NewGuid(),
                Name = "EMP0021",
                NodeTypeId = employeeNodeType.Id,
                ParentId = companyNode.Id
            };

            context.Nodes.AddOrUpdate(employeePresident, p => p.ParentId, p => p.Name);



            context.Properties.AddOrUpdate(new Property()
            {
                Id = Guid.NewGuid(),
                NodeId = employeePresident.Id,
                PropertyTypeId = employeeFirstNamePropertyType.Id,
                Value = Newtonsoft.Json.JsonConvert.SerializeObject("Andrew")
            }, p => p.NodeId, p => p.PropertyTypeId);


            context.Properties.AddOrUpdate(new Property()
            {
                Id = Guid.NewGuid(),
                NodeId = employeePresident.Id,
                PropertyTypeId = emaployeeLastNamePropertyType.Id,
                Value = Newtonsoft.Json.JsonConvert.SerializeObject("Fuller")
            }, p => p.NodeId, p => p.PropertyTypeId);


            context.Properties.AddOrUpdate(new Property()
            {
                Id = Guid.NewGuid(),
                NodeId = employeePresident.Id,
                PropertyTypeId = employeeTitlePropertyType.Id,
                Value = Newtonsoft.Json.JsonConvert.SerializeObject("Vice President")
            }, p => p.NodeId, p => p.PropertyTypeId);

            context.Properties.AddOrUpdate(new Property()
            {
                Id = Guid.NewGuid(),
                NodeId = employeePresident.Id,
                PropertyTypeId = employeeExtensionPropertyType.Id,
                Value = Newtonsoft.Json.JsonConvert.SerializeObject(3457)
            }, p => p.NodeId, p => p.PropertyTypeId);


            context.Properties.AddOrUpdate(new Property()
            {
                Id = Guid.NewGuid(),
                NodeId = employeePresident.Id,
                PropertyTypeId = employeeDobPropertyType.Id,
                Value = Newtonsoft.Json.JsonConvert.SerializeObject(DateTime.Parse("1980-10-14"))
            }, p => p.NodeId, p => p.PropertyTypeId);



            #endregion

            #region Employee 2
            Node employeeManager = new Node()
            {
                Id = Guid.NewGuid(),
                Name = "EMP0054",
                NodeTypeId = employeeNodeType.Id,
                ParentId = employeePresident.Id
            };

            context.Nodes.AddOrUpdate(employeeManager, p => p.ParentId, p => p.Name);

            context.Properties.AddOrUpdate(new Property()
            {
                Id = Guid.NewGuid(),
                NodeId = employeeManager.Id,
                PropertyTypeId = employeeFirstNamePropertyType.Id,
                Value = Newtonsoft.Json.JsonConvert.SerializeObject("Steven")
            }, p => p.NodeId, p => p.PropertyTypeId);

            context.Properties.AddOrUpdate(new Property()
            {
                Id = Guid.NewGuid(),
                NodeId = employeeManager.Id,
                PropertyTypeId = emaployeeLastNamePropertyType.Id,
                Value = Newtonsoft.Json.JsonConvert.SerializeObject("Buchanan")
            }, p => p.NodeId, p => p.PropertyTypeId);

            context.Properties.AddOrUpdate(new Property()
            {
                Id = Guid.NewGuid(),
                NodeId = employeeManager.Id,
                PropertyTypeId = employeeTitlePropertyType.Id,
                Value = Newtonsoft.Json.JsonConvert.SerializeObject("Sales Manager")
            }, p => p.NodeId, p => p.PropertyTypeId);

            context.Properties.AddOrUpdate(new Property()
            {
                Id = Guid.NewGuid(),
                NodeId = employeeManager.Id,
                PropertyTypeId = employeeExtensionPropertyType.Id,
                Value = Newtonsoft.Json.JsonConvert.SerializeObject(3453)
            }, p => p.NodeId, p => p.PropertyTypeId);

            context.Properties.AddOrUpdate(new Property()
            {
                Id = Guid.NewGuid(),
                NodeId = employeeManager.Id,
                PropertyTypeId = employeeDobPropertyType.Id,
                Value = Newtonsoft.Json.JsonConvert.SerializeObject(DateTime.Parse("1982-06-08"))
            }, p => p.NodeId, p => p.PropertyTypeId);



            #endregion

            #region Employee 3
            Node employeeRepresentative1 = new Node()
            {
                Id = Guid.NewGuid(),
                Name = "EMP0124",
                NodeTypeId = employeeNodeType.Id,
                ParentId = employeeManager.Id
            };

            context.Nodes.AddOrUpdate(employeeRepresentative1, p => p.ParentId, p => p.Name);

            context.Properties.AddOrUpdate(new Property()
            {
                Id = Guid.NewGuid(),
                NodeId = employeeRepresentative1.Id,
                PropertyTypeId = employeeFirstNamePropertyType.Id,
                Value = Newtonsoft.Json.JsonConvert.SerializeObject("Nancy")
            }, p => p.NodeId, p => p.PropertyTypeId);

            context.Properties.AddOrUpdate(new Property()
            {
                Id = Guid.NewGuid(),
                NodeId = employeeRepresentative1.Id,
                PropertyTypeId = emaployeeLastNamePropertyType.Id,
                Value = Newtonsoft.Json.JsonConvert.SerializeObject("Davolio")
            }, p => p.NodeId, p => p.PropertyTypeId);

            context.Properties.AddOrUpdate(new Property()
            {
                Id = Guid.NewGuid(),
                NodeId = employeeRepresentative1.Id,
                PropertyTypeId = employeeExtensionPropertyType.Id,
                Value = Newtonsoft.Json.JsonConvert.SerializeObject(5467)
            }, p => p.NodeId, p => p.PropertyTypeId);

            context.Properties.AddOrUpdate(new Property()
            {
                Id = Guid.NewGuid(),
                NodeId = employeeRepresentative1.Id,
                PropertyTypeId = employeeTitlePropertyType.Id,
                Value = Newtonsoft.Json.JsonConvert.SerializeObject("Sales Representative")
            }, p => p.NodeId, p => p.PropertyTypeId);

            context.Properties.AddOrUpdate(new Property()
            {
                Id = Guid.NewGuid(),
                NodeId = employeeRepresentative1.Id,
                PropertyTypeId = employeeDobPropertyType.Id,
                Value = Newtonsoft.Json.JsonConvert.SerializeObject(DateTime.Parse("1990-10-05"))
            }, p => p.NodeId, p => p.PropertyTypeId);



            #endregion

            #region Employee 4
            Node employeeRepresentative2 = new Node()
            {
                Id = Guid.NewGuid(),
                Name = "EMP0136",
                NodeTypeId = employeeNodeType.Id,
                ParentId = employeeManager.Id
            };

            context.Nodes.AddOrUpdate(employeeRepresentative2, p => p.ParentId, p => p.Name);

            context.Properties.AddOrUpdate(new Property()
            {
                Id = Guid.NewGuid(),
                NodeId = employeeRepresentative2.Id,
                PropertyTypeId = employeeFirstNamePropertyType.Id,
                Value = Newtonsoft.Json.JsonConvert.SerializeObject("Margaret")
            }, p => p.NodeId, p => p.PropertyTypeId);

            context.Properties.AddOrUpdate(new Property()
            {
                Id = Guid.NewGuid(),
                NodeId = employeeRepresentative2.Id,
                PropertyTypeId = emaployeeLastNamePropertyType.Id,
                Value = Newtonsoft.Json.JsonConvert.SerializeObject("Peacock")
            }, p => p.NodeId, p => p.PropertyTypeId);
            context.Properties.AddOrUpdate(new Property()
            {
                Id = Guid.NewGuid(),
                NodeId = employeeRepresentative2.Id,
                PropertyTypeId = employeeTitlePropertyType.Id,
                Value = Newtonsoft.Json.JsonConvert.SerializeObject("Sales Representative")
            }, p => p.NodeId, p => p.PropertyTypeId);

            context.Properties.AddOrUpdate(new Property()
            {
                Id = Guid.NewGuid(),
                NodeId = employeeRepresentative2.Id,
                PropertyTypeId = employeeExtensionPropertyType.Id,
                Value = Newtonsoft.Json.JsonConvert.SerializeObject(5176)
            }, p => p.NodeId, p => p.PropertyTypeId);

            context.Properties.AddOrUpdate(new Property()
            {
                Id = Guid.NewGuid(),
                NodeId = employeeRepresentative2.Id,
                PropertyTypeId = employeeDobPropertyType.Id,
                Value = Newtonsoft.Json.JsonConvert.SerializeObject(DateTime.Parse("1984-08-03"))
            }, p => p.NodeId, p => p.PropertyTypeId);



            #endregion

            #endregion




            context.NodeTypeAllowedNodeTypes.AddOrUpdate(new NodeTypeAllowedNodeType() { NodeTypeId = organizationNodeType.Id, AllowedNodeTypeId = employeeNodeType.Id }, p => p.NodeTypeId, prop => prop.AllowedNodeTypeId);
            context.NodeTypeAllowedNodeTypes.AddOrUpdate(new NodeTypeAllowedNodeType() { NodeTypeId = employeeNodeType.Id, AllowedNodeTypeId = employeeNodeType.Id }, p => p.NodeTypeId, prop => prop.AllowedNodeTypeId);

            //context.NodeTypeAllowedNodeTypes.AddOrUpdate(new NodeTypeAllowedNodeType() { NodeTypeId = countryNodeType.Id, AllowedNodeTypeId = cityNodeType.Id }, p => p.NodeTypeId, prop => prop.AllowedNodeTypeId);


            context.SaveChanges();

        }

    }
}
