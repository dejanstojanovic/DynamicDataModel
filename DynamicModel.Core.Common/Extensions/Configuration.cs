﻿using Microsoft.Extensions.Configuration;
using System;

namespace DynamicModel.Core.Common.Extensions
{
    public static class Configuration
    {
        public static T Get<T>(this IConfiguration configuration,String key) where T:IConvertible
        {
           return (T)Convert.ChangeType(configuration[key], typeof(T));
        }
    }
}
